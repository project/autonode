<?php

/**
 * @file
 * Defines a field type for autoreferencing one node from another.
 * Autoreference: if typed node doesnt exist it will create one according
 * to the settings
 */

/**
 * Implementation of hook_menu() based nodereference_menu
 */
function autonode_menu($may_cache) {
  $items = array();

  if ($may_cache) {
    $items[] = array('path' => 'autonode/autocomplete', 'title' => t('autonode reference autocomplete'),
      'callback' => 'autonode_autocomplete', 'access' => user_access('access content'), 'type' => MENU_CALLBACK);
  }

  return $items;
}

/**
 * Implementation of hook_field_info()
 */
function autonode_field_info() {
  return array(
    'autonode' => array('label' => 'Autonode Reference'),
  );
}


/**
 * Implementation of hook_field_settings()
 */
function autonode_field_settings($op, $field) {
  switch ($op) {
  case 'database columns':
    $columns = array(
      'code_types' => array(
        'type' => 'varchar',
        'not null' => TRUE,
        'default' => "''",
        'sortable' => TRUE,
      ),
      'nid' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => '0',
      ),
    );
    $columns['code_types']['length'] = isset($field['maxlength']) ? $field['maxlength'] : 12; //TODO make this dependant on the type of code we have in
    return $columns;

  case 'form':
    $form = array();
    $form['referenceable_types'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Content types that can be referenced'),
      '#multiple' => TRUE,
      '#required' => TRUE,
      '#default_value' => isset($field['referenceable_types']) ? $field['referenceable_types'] : array(),
      '#options' => node_get_types('names'),
    );
    $form['code_types']['#tree'] = TRUE;
    $form['code_types']['airport'] = array(
      '#collapsible' => TRUE,
      '#type' => 'fieldset',
      '#title' => t('Airport code types'),
      '#description' => t('Select which airport code do you want to use for reference.'),
    );
    $form['code_types']['airport']['codes'] = array(
      '#type' => 'radios',
      '#required' => TRUE,
      '#title' => t('Available code types'),
      '#default_value' => $field['code_types']['airport']['codes'] ? $field['code_types']['airport']['codes'] : '',
      '#options' => array( 
        'iata' => t('3 letter IATA code (BUD, WAW etc)'),
        'icao' => t('4 letter ICAO code (LHBP, EPWA etc)'),
      ),
    );
  
    return $form;
  case 'save':
    return array('referenceable_types', 'code_types', );
  }
}

// hook_field

function autonode_field($op, &$node, $field, &$items, $teaser, $page) {
  switch($op) {
  case 'validate':
    foreach ($items as $delta => $item) {
      $error_field = $field['field_name'] .']['. $delta .'][node_name';
      if($item['nid'] == -1) {
        form_set_error($error_field, t('%name : this %code doesn\'t exist in the main database. Please contact the administrator so we can add it.', array('%name' => t($item['node_name']), '%code' => t($field['widget']['label']) ) ));
      }
    }
    return;
  case 'submit':
    foreach($items as $delta => $item) {
      if($nid == 0) { /* then we successfully assigned in widget/process */
        autonode_create_node($item['node_name'], $setup, 1);
      }
    }
  }
}


/**
 * Implementation of hook_field_formatter_info().
 */
function autonode_field_formatter_info() {
  return array(
    'default' => array(
      'label' => 'Default',
      'field types' => array('autonode'),
    ),
    'plain' => array(
      'label' => 'Plain text',
      'field types' => array('autonode'),
    ),
  );
}


/**
 * Implementation of hook_field_formatter().
 */
function autonode_field_formatter($field, $item, $formatter, $node) {
  $text = '';
  if (!empty($item['nid'])) {
    $referenced_node = node_load($item['nid']);
    if ($referenced_node) {
      $text = l($referenced_node->title, 'node/'. $referenced_node->nid);
    }
  }

  switch ($formatter) {
    case 'plain':
      return strip_tags($text);

    default:
      return $text;
  }
}


// hook_widget_info

function autonode_widget_info () {
  return array(
    'airport_codes' => array(
      'label' => t('Define by airport codes'),
      'field types' => array('autonode', ), 
/** note the space beween field and types 
 * otherwise content_admin.inc errors 
 */
    ),
  );
} 

// hook_widget_settings
/**
 * comment to self: widget_settings cant reach outside to check the whole field. 
 * $widget is going to look like :
 * Array
 * (
 *    [default_value] => 
 *    [default_value_php] => 
 *    [maxlength] => 12
 *    [type] => airport_codes
 *    [weight] => 0
 *    [label] => Airport Codefield
 *    [description] => 
 *  )
 * Here we cant just grab a maxlength according to what type of widget we picked. 
 * this can be done in widget(), because then we receive the $field variable
 */

/**
 * Implementation of hook_widget
 */

function autonode_widget($op, &$node, $field, &$items) {
  switch($op) {
  case 'prepare form values':
    foreach ($items as $delta => $item) {
      if (isset($items[$delta]['nid'])) { //empty() will be true for 0
        $val = db_result(db_query(db_rewrite_sql('SELECT n.title FROM {node} n WHERE n.nid = %d'), $items[$delta]['nid']));
        $items[$delta]['default node_name'] = !(isset($val)) || $val == 0 ? 'NAN' : $val;
      }
    }
    break;

  case 'form' :
    $code = $field['code_types']['airport']['codes'];
    switch($code) { // hardcoded widget length
    case 'iata':
      $length = 3;
      break;
    case 'icao':
      $length = 4;
      break;
    default: 
      $length = 12;
      break;
    }

    $form = array();
    $form[$field['field_name']] = array('#tree' => TRUE); // This is important. delete it, and you're dead
    $form[$field['field_name']][0]['node_name'] = array(
      '#type' => 'textfield',
      '#title' => t($field['widget']['label'] . ', ' . strtoupper($code) ),
      '#autocomplete_path' => 'autonode/autocomplete/'.$field['field_name'],
      '#default_value' => $items[0]['default node_name'],
      '#maxlength' => $length,
      '#size' => $length+1,
      '#required' => $field['required'],
      '#description' => t($field['widget']['description']),
    );
    return $form;
  case 'validate':
    /*foreach ($items as $delta => $item) {
      $error_field = $field['field_name'] .']['. $delta .'][node_name';

      if(!empty($item['node_name'])) {
        if(autonode_create_node($item['node_name'], $setup, 0) == '-1') { // we pass what's in the field
          form_set_error($error_field, t('%name : this %code doesn\'t exist in the main database. Please contact the administrator so we can add it.', array('%name' => t($item['node_name']), '%code' => t($field['widget']['label']) ) ));
        }
      }
    }*/
    return;
  case 'process form values': 
    /* here we assign the nid to $items */
    foreach ($items as $delta => $item) {
      if (!empty($item['node_name'])) {
        $nid = autonode_create_node($item['node_name'], $setup, 0);
      }
      if($nid != "") { /* if we succeeded in assigning a $nid above */
        $items[$delta]['nid'] = $nid;
        $items[$delta]['error_field'] = $field['field_name'] . '][' .$delta . '][node_name';
        $items[$delta]['code_types'] = $field['code_types']['airport']['codes'];
      }
      //unset($items[$delta]['node_name']); // we need the representation
      elseif($delta>0) {
        unset($items[$delta]);
      }
    }
    break;
  }
}

function autonode_autocomplete($field_name, $string = '') {
  $fields = content_fields();
  $field = $fields[$field_name];
  $matches = array();

  foreach (_nodereference_potential_references( $field, TRUE, $string) as $row) {
    $matches[$row->node_title] = _nodereference_item($field, $row, TRUE);
  }
  print drupal_to_js($matches);
  exit();
}

/**
 * This function handles the automatic node creation. 
 * $entry determines if it is a real entry, or just a test.
 * creates a node using $title as unique identifier in the reference database
 *
 * if $entry is true, 
 * return -1: means the referred node doesnt exist in the reference database
 *         0: we are to write the first article,
 *         nid: existing reference, returns nid
 *
 * $setup['type']: the type column in {node} for our content type
 */
function autonode_create_node($title, $setup, $entry) { //TODO: $title -> $field_name
  $setup = array();
  $setup['type'] = 'story'; //this should be like in nodereference, referenceable_types
  $title = strtoupper($title); //airport code specific, they are capital letters

  
  if($entry) { /* we create an entry if necessary */
    if(autonode_create_node($title, $setup, 0) == -1) { 
      //this is deep shit, should be filtered out with validate
      return;
    }

    if(autonode_create_node($title, $setup, 0) == 0) { //test if we create, or just refer
      $node = array('type' => $setup['type']); 
      $values['title'] = $title; 
      $values['body'] = 'This is the body text!'; 
      $values['name'] = 'drupal'; 
      drupal_execute('story_node_form', $values, $node);
      $nid = autonode_query_nid($title, $setup['type']);
    }

    else { /* we asked for insertion, but no need, just refer. return the refer nid */
      $nid = autonode_query_nid($title, $setup['type']);
    }

  }

  else { /* $entry = 0, checking if entry we try to refer exists */
    $nid =  autonode_query_nid($title, $setup['type']);
    $nid = $nid ? $nid : in_database($title);  /* real nid if exists, 0 if new, -1 if not in db */
  }
  return($nid);
}

/**
 * custom sql query 
 * returns the nid by title, type. 
 * if nothing found $nid = ""
 * this should be used only if we are looking for ONE nid.
 */
function autonode_query_nid($title, $type) {
  $nid = db_result(db_query(db_rewrite_sql("SELECT n.nid FROM {node} n WHERE n.title='%s' AND n.type='%s'"), $title, $type )); 
  return $nid;
}

/**
 * check the imported reference database for entries
 * we should get here only if there is no entry in the 
 * drupal database already
 */
function in_database($id) {
  $database = array('WAW', 'BUD', 'AAA');
  if(in_array($id, $database)) {
    return(0);
  }
  else {
    return(-1);
  }
}
